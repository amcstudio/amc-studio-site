$(function() {

	$('a[rel=external], #social a').attr('target', '_blank');

	$('#team a[data-fancybox]').fancybox({
		loop    : true,
		infobar : false,
		caption : function(instance, item) {
			return $('span', this).html();
		}
	});

	$('#terms a').fancybox({
		type: 'iframe'
	});

	$('#menu').click(function() {
		$(this).next().toggleClass('mobile-hidden');
	});

	$('#top').click(function() {
		$('html, body').scrollTop(0);
	});

	if ($(window).width() < 768) FastClick.attach(document.body);

});


function initMap() {

	var position = {lat: 18.5703032, lng: 73.9106763};
	
	var map = new google.maps.Map(document.getElementById('map'), {
	  zoom: 18,
	  center: position,
	  mapTypeControl: false,
	  streetViewControl: true,
	  scrollwheel: false
	});
	
	var icon = {
	  url: '../images/map-icon.png',
	  scaledSize: new google.maps.Size(26, 40),
	  origin: new google.maps.Point(0, 0),
	  anchor: new google.maps.Point(0, 40)
	};
	
	var marker = new google.maps.Marker({
	  map: map, 
	  icon: icon,
	  position: position
	});
	
	var html = "<div style='font:12px/1 Montserrat;padding:0.3em'><b>AMC Studio</b><br/>404, Konark Epitom, Mhada Colony, Viman Nagar,<br/>Pune, Maharashtra 411014, India</div>";
	var infoWindow =  new google.maps.InfoWindow();
	infoWindow.setContent(html);
	infoWindow.open(map, marker);
	
	google.maps.event.addListener(marker, 'click', function() {
	  infoWindow.open(map, marker);
	});
	}